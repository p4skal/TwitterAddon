package de.p4skal.twitter.controllers;

import com.google.common.io.Closer;
import com.google.common.util.concurrent.ListenableScheduledFuture;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.p4skal.twitter.TwitterAddon;
import de.p4skal.twitter.entities.FollowerResponse;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Handle Twitter Follower API Access. Contain Cron Job to update Twitter Account Information.
 * Prepare Data for {@link de.p4skal.twitter.modules.TwitterFollowerModule}.
 *
 * @author p4skal
 */
@Getter
@RequiredArgsConstructor
public class TwitterController {
    /**
     * Main Class of the Addon.
     */
    private final TwitterAddon twitterAddon;
    /**
     * ExecutorService to provide Cron Job.
     */
    private final ListeningScheduledExecutorService executorService = MoreExecutors.listeningDecorator(
            Executors.newSingleThreadScheduledExecutor()
    );
    /**
     * Instance the Twitter Updater Runnable.
     */
    private final TwitterUpdater twitterUpdater = new TwitterUpdater();
    /**
     * Reference to the Cron Job.
     */
    private ListenableScheduledFuture updater;

    /**
     * (Re-)start Cron Job.
     */
    public void startUpdater( ) {
        // Check if Update exist.
        if ( this.updater != null )
            // Shutdown current updater.
            this.updater.cancel( true );

        // Create new Updater with current Properties.
        this.updater = this.getExecutorService().scheduleAtFixedRate(
                this.twitterUpdater,
                0L,
                this.twitterAddon.getTwitterFollowerModule().getRequestInterval(),
                TimeUnit.SECONDS
        );
    }

    /**
     * Stop Cron Job, shutdown ThreadPool.
     */
    public void stopAll( ) {
        // Check if Update exist.
        if ( this.updater != null ) {
            // Shutdown current updater.
            this.updater.cancel( true );
            // Un-reference current updater.
            this.updater = null;
        }

        // Shutdown Executor Service.
        this.executorService.shutdown();

        try {
            // Close API Client.
            this.twitterUpdater.getCloser().close();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Represent the Update Task. Contain API Access.
     */
    @Getter
    private class TwitterUpdater implements Runnable {
        /**
         * Wrapper contain all Instances (HttpClient, etc.) to Close.
         */
        private final Closer closer = Closer.create();
        /**
         * Gson instance to deserialize API Response.
         */
        private final Gson gson = new GsonBuilder().create();
        /**
         * The Http Client.
         */
        private final CloseableHttpClient httpClient = HttpClients.createDefault();
        /**
         * The API URL. %s = Twitter Name
         */
        private final String url = "https://cdn.syndication.twimg.com/widgets/followbutton/info.json?screen_names=%s";
        /**
         * Contain time of last Request, to prevent API Block in case of Exception.
         */
        private AtomicLong lastRequest = new AtomicLong();

        /**
         * {@inheritDoc}
         */
        @Override
        public void run( ) {
            // Check for min. Delay.
            if ( ( System.currentTimeMillis() - this.lastRequest.get() ) < 2000 )
                return;

            // Create Get Request.
            final HttpGet httpGet = new HttpGet(
                    String.format(
                            this.url, TwitterController.this.twitterAddon.getTwitterFollowerModule().getTwitterName()
                    )
            );

            // Define Response.
            CloseableHttpResponse httpResponse = null;

            try {
                // Make API Response.
                httpResponse = this.httpClient.execute( httpGet );
                // Parse JSON Response to POJOs.
                FollowerResponse[] followerResponses = this.gson.fromJson(
                        IOUtils.toString( httpResponse.getEntity().getContent() ), FollowerResponse[].class
                );

                // Check for no Result.
                if ( followerResponses.length == 0 )
                    // Set Followers to not shown.
                    TwitterController.this.twitterAddon.getTwitterFollowerModule().getFollowers().set( -1 );
                else
                    // Update Followers to requested Followers.
                    TwitterController.this.twitterAddon.getTwitterFollowerModule().getFollowers().set(
                            followerResponses[0].getFollowersCount()
                    );

            } catch ( IOException e ) {
                e.printStackTrace();
            } finally {
                try {
                    // Check if HttpResponse exist.
                    if ( httpResponse != null )
                        // Close HttpResponse.
                        httpResponse.close();
                } catch ( IOException e ) {
                    e.printStackTrace();
                }
            }
        }
    }
}
