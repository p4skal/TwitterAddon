package de.p4skal.twitter;

import de.p4skal.twitter.controllers.TwitterController;
import de.p4skal.twitter.modules.TwitterFollowerModule;
import lombok.Getter;
import net.labymod.api.LabyModAddon;
import net.labymod.settings.elements.SettingsElement;

import java.util.List;

/**
 * Main Entry-Point for LabyMod.
 *
 * TODO:
 * - may add live api (e.g. social blade parser / twitter api)
 * - may change module icon
 *
 * @author p4skal
 */
@Getter
public class TwitterAddon extends LabyModAddon {
    /* --CONTROLLERS-- */
    private TwitterController twitterController;

    /* --MODULES-- */
    private TwitterFollowerModule twitterFollowerModule;

    /**
     * Called when Addon get's enabled.
     */
    @Override
    public void onEnable( ) {
        /* --CONTROLLERS-- */

        // Create TwitterController.
        this.twitterController = new TwitterController( this );

        /* --MODULES-- */

        // Register Twitter Follower Module.
        this.getApi().registerModule( this.twitterFollowerModule = new TwitterFollowerModule( this ) );

    }

    /**
     * Called when Addon get's disabled.
     */
    @Override
    public void onDisable( ) {
        // Stop Twitter updater.
        this.twitterController.stopAll();
    }

    /**
     * Called when Config get's load.
     */
    @Override
    public void loadConfig( ) {
    }

    /**
     * Called when the addon's Ingame-Settings should be filled.
     *
     * @param subSettings A list containing the addon's settings' elements.
     */
    @Override
    protected void fillSettings( List<SettingsElement> subSettings ) {
    }
}
