package de.p4skal.twitter.entities;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represent API Response as Bean / POJO.
 *
 * @author p4skal
 */
@Data
@NoArgsConstructor
public class FollowerResponse {
    /**
     * Ignored.
     */
    private boolean following;
    /**
     * Ignored.
     */
    private long id;
    /**
     * The Display Name of the Twitter Account.
     */
    @SerializedName( "screen_name" )
    private String screenName;
    /**
     * The actual Name of the Twitter Account.
     */
    private String name;
    /**
     * The protection State of the Account.
     */
    @SerializedName( "protected" )
    private boolean protectedState;
    /**
     * Current Followers of the Account.
     */
    @SerializedName( "followers_count" )
    private int followersCount;
}
