package de.p4skal.twitter.modules;

import de.p4skal.twitter.TwitterAddon;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.labymod.ingamegui.moduletypes.SimpleModule;
import net.labymod.settings.elements.ControlElement;
import net.labymod.settings.elements.NumberElement;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.settings.elements.StringElement;
import net.labymod.utils.Consumer;
import net.labymod.utils.Material;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Represent the Twitter Follower Module. Handle Design / UI & Configuration of Module.
 *
 * @author p4skal
 */
@Getter
@RequiredArgsConstructor
public class TwitterFollowerModule extends SimpleModule {
    /**
     * Main Class of the Addon.
     */
    private final TwitterAddon twitterAddon;
    /**
     * The IconData of the Module.
     */
    private final ControlElement.IconData iconData = new ControlElement.IconData( Material.DIAMOND );
    /**
     * The Twitter Name.
     */
    private String twitterName;
    /**
     * The request Interval to update.
     */
    private long requestInterval;
    /**
     * Current Followers of the {@link #twitterName}.
     */
    private AtomicInteger followers = new AtomicInteger( -1 );

    /**
     * Called when the IconData of the Module get's loaded.
     *
     * @return The IconData.
     */
    @Override
    public ControlElement.IconData getIconData( ) {
        return this.iconData;
    }

    /**
     * Called when Module get's drawn.
     *
     * @return True if Module should be visible.
     */
    @Override
    public boolean isShown( ) {
        return this.followers.get() != -1;
    }

    /**
     * Called when Settings should be loaded.
     * Fill properties with Config values.
     * Start updater Cron Job.
     */
    @Override
    public void loadSettings( ) {
        // Fill Name.
        this.twitterName = this.getAttribute( "twitter_name", "p4skal" );

        // Get Interval.
        int requestInterval = Integer.parseInt( this.getAttribute( "twitter_interval", "30" ) );
        // Check if Request Interval is smaller then 30 seconds.
        if ( requestInterval < 30 ) requestInterval = 30;
        // Fill Interval.
        this.requestInterval = requestInterval;

        // Start Updater.
        twitterAddon.getTwitterController().startUpdater();
    }

    /**
     * Called when Settings GUI get's build up.
     * Adjust Module Settings.
     *
     * @param settingsElements All SettingElements of this Module.
     */
    @Override
    public void fillSubSettings( List<SettingsElement> settingsElements ) {
        // Call parent Fill Sub Settings Method.
        super.fillSubSettings( settingsElements );

        // Add Twitter Name Element.
        settingsElements.add(
                new StringElement(
                        this, new ControlElement.IconData( Material.PAPER ), "Twitter Name (ohne @)", "twitter_name"
                ).maxLength( 100 )
                        .addCallback( new Consumer<String>() {
                            @Override
                            public void accept( String name ) {
                                // Set new Name.
                                TwitterFollowerModule.this.twitterName = name;
                            }
                        } )
        );

        // Add Update Interval Element.
        settingsElements.add(
                new NumberElement(
                        this, new ControlElement.IconData( Material.WATCH ), "Update interval", "twitter_interval"
                ).setRange( 30, 500 )
                        .addCallback( new Consumer<Integer>() {
                            @Override
                            public void accept( Integer value ) {
                                // Set new Request Interval.
                                TwitterFollowerModule.this.requestInterval = value;
                            }
                        } )
        );
    }

    /**
     * Get the Control Name of the Module.
     *
     * @return The Control Name as String.
     */
    @Override
    public String getControlName( ) {
        return "Twitter Followers";
    }

    /**
     * Get the Setting Name of the Module.
     *
     * @return The Setting Name as String.
     */
    @Override
    public String getSettingName( ) {
        return "twitter_followers";
    }

    /**
     * Get the Description of the Module.
     *
     * @return The Description as String.
     */
    @Override
    public String getDescription( ) {
        return "Show Twitter Followers.";
    }

    /**
     * Get sorting of the Module.
     *
     * @return SortingId as int.
     */
    @Override
    public int getSortingId( ) {
        return 0;
    }

    /**
     * Get Key of the Entry to Display.
     *
     * @return The Key as String.
     */
    @Override
    public String getDisplayName( ) {
        return "Follower";
    }

    /**
     * Get Value of the Entry to Display.
     *
     * @return The Value as String.
     */
    @Override
    public String getDisplayValue( ) {
        return String.valueOf( this.followers.get() );
    }

    /**
     * The Entry-Value that will be shown if {@link #isShown()} return {@code false}.
     *
     * @return The Default Value as String.l
     */
    @Override
    public String getDefaultValue( ) {
        return "";
    }
}
